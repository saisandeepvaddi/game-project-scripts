// this script should make the gun point at where the mouse is currently positioned above

function Update ()

{

var aimPoint : Vector3;
//var gun : GameObject;
var closestDistFromCamera : float = Mathf.Infinity;

var hits : RaycastHit[];

hits = Physics.RaycastAll(Camera.current.ScreenPointToRay(Input.mousePosition));

for (var i = 0;i < hits.Length; i++) {

    var hit : RaycastHit = hits[i];

    if (Vector3.Distance(hit.point, Camera.current.ScreenPointToRay(Input.mousePosition).origin) < closestDistFromCamera){

          aimPoint = hit.point;

          closestDistFromCamera = Vector3.Distance(hit.point, Camera.current.ScreenPointToRay(Input.mousePosition).origin);}

}

transform.rotation = Quaternion.LookRotation(aimPoint - transform.position);

}