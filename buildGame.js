#pragma strict
import System.IO;

internal var filePath : String;
internal var extension = ".gme";

function Start () {
	filePath = Application.dataPath+"/";
	createLevels();
}


function createLevels(){
if(!File.Exists(filePath + "levels" + extension)){
	var sWrite: StreamWriter = new StreamWriter(filePath + "levels" + extension);
	var level : int = 1;
	sWrite.WriteLine(level);
	sWrite.Flush();
	sWrite.Close();
	}else{
		return;
	}
}