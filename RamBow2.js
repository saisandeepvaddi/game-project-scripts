var arrowPrefab : Transform;
var rocketSpeed : float = 40;
var spawnPoint : Transform;


function Update()
{

	if(Input.GetButtonUp("Fire1"))
	{
		var ray : Ray = Camera.main.ViewportPointToRay(Vector3(0.5,0.5,0));
		var hit : RaycastHit;
		var dir : Vector3;
		if(Physics.Raycast(ray,hit))
		{
			dir = (hit.point-spawnPoint.position).normalized;
		
		}
		else
		{
			dir = ray.direction;
		}
		var rot = Quaternion.FromToRotation(arrowPrefab.forward,dir);
		
			var rocket = Instantiate(arrowPrefab,spawnPoint.position,rot);
			rocket.rigidbody.velocity = dir*rocketSpeed;
	}

}