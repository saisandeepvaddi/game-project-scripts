#pragma strict
import System.IO;

internal var filePath : String;
internal var extension = ".gme";

function Start () {
	filePath = Application.dataPath+"/";
}

function Update () {

}

var bg : Texture;
var title : Texture;
var buttonStyle : GUIStyle;
var lockStyle : GUIStyle;
function OnGUI(){
	var levels : int=ReadLevels();
	var i : int;
	GUI.DrawTexture(Rect(Screen.width/2 - 350, Screen.height/2 - 270,660,450), bg);
	GUI.DrawTexture(Rect (Screen.width/2 - 130, Screen.height/2 - 260,280,60), title);
	var top : int = 200;
	for(i=1;i<=levels;i++){
   	GUI.skin.button = buttonStyle;
	   	if(GUI.Button(Rect(Screen.width/2 - 60, Screen.height/2 - top,130,40),"Level "+i)){
	   		Application.LoadLevel(3+i);
	   	
	   	}top=top-50;
   	}
   	
   	GUI.skin.button = lockStyle;
   	for(i=i;i<=4;i++){
   	GUI.Button(Rect(Screen.width/2 - 60, Screen.height/2 - top,130,40),"Level "+i);
   	top=top-50;
   	}
	
	GUI.skin.button = buttonStyle;
	if(GUI.Button(Rect(Screen.width/2 - 300, Screen.height/2 + 80 ,70,40),"Back")){
		Application.LoadLevel(2);
	}
}


function ReadLevels(){
if (!File.Exists(filePath + "levels" + extension)) return;
	var sRead = new File.OpenText(filePath + "levels" + extension);
	var level=sRead.ReadLine();
	sRead.Close();
	if(level[0]=="1")
		return 1;
	if(level[0]=="2")
		return 2;
	if(level[0]=="3")
		return 3;
		
}