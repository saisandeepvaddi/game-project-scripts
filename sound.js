#pragma strict

@script RequireComponent (AudioSource)

var onlyPlayOnce : boolean = true;
var hSliderValue : float = 0.3;

private var playedOnce : boolean = false;

function OnTriggerEnter (unusedArg) {
	if (playedOnce && onlyPlayOnce)
		return;
	audio.Play ();
	AudioListener.volume =hSliderValue;
	playedOnce = true;
}