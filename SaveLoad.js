#pragma strict
import System.IO;

internal var filePath : String;
internal var filename = "SavedGame";
internal var extension = ".gme";

function Start () {
	filePath = Application.dataPath+"/";
}

function WriteFile(filename : String) {
	var sWrite: StreamWriter = new StreamWriter(filePath + filename + extension);
	var fpc = GameObject.Find("Camera");
	sWrite.WriteLine(fpc.transform.position);
	sWrite.WriteLine(fpc.transform.localEulerAngles);
	sWrite.Flush();
	sWrite.Close();
}

function WriteProfile(name : String) {
	var sWrite: StreamWriter = new StreamWriter(filePath + "profile" + extension);
	sWrite.WriteLine(name);
	sWrite.Flush();
	sWrite.Close();
}

public var profile : String = ReadProfile();
function ReadProfile(){
if (!File.Exists(filePath + "profile" + extension)) return;
	var sRead = new File.OpenText(filePath + "profile" + extension);
	var profil=sRead.ReadLine();
	sRead.Close();
	return profil;
}

function ReadFile(fileName : String) {
	if (!File.Exists(filePath + fileName + extension)) return;
	var sRead = new File.OpenText(filePath + fileName + extension);
	var fpc = GameObject.Find("Camera");
	ProcessTransforms(fpc,sRead.ReadLine(),"position");
	ProcessTransforms(fpc,sRead.ReadLine(),"rotation");
	sRead.Close();
}

function ProcessTransforms (object :GameObject, theValue : String, transform : String) {
	theValue = theValue.Substring(1,theValue.length -2);
	var readString : String[] = theValue.Split(","[0]);
	var nt : Vector3 =
	Vector3(parseFloat(readString[0]),parseFloat(readString[1]),parseFloat(readString[2]));
	if (transform == "position") object.transform.position = nt;
	else object.transform.localEulerAngles = nt;
}