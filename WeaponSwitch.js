#pragma strict

function Awake()
{
	SelectWeapon(0);


}

function Update () {

	if(Input.GetKeyDown("1"))
	{
		Debug.Log("Weapon 1 Selected");	
		SelectWeapon(0);
	}
	else if(Input.GetKeyDown("2"))
	{
		Debug.Log("Weapon 2 Selected");
		SelectWeapon(1);
	}
}

function SelectWeapon(index : int)
{
	for(var i = 0; i<transform.childCount; i++)
	{
		if(i == index)
			transform.GetChild(i).gameObject.SetActiveRecursively(true);
		else
			transform.GetChild(i).gameObject.SetActiveRecursively(false);
	}

}