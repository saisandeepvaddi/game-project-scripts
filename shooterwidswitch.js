private var arrowPrefab : GameObject;
var rocketSpeed : float = 20;
var spawnPoint : Transform;
private var weaponCount : int = 2;

function Awake()
{
	SelectWeapon(0);

}

function Update()
{
	if(Input.GetKeyDown("1"))
	{
		SelectWeapon(0);
	}
	else if (Input.GetKeyDown("2"))
	{
		SelectWeapon(1);
	
	}
	if(Input.GetButtonUp("Fire1"))
	{
		var ray : Ray = Camera.main.ViewportPointToRay(Vector3(0.5,0.5,0));
		var hit : RaycastHit;
		var dir : Vector3;
		if(Physics.Raycast(ray,hit))
		{
			dir = (hit.point-spawnPoint.position).normalized;
		
		}
		else
		{
			dir = ray.direction;
		}
		var rot = Quaternion.FromToRotation(arrowPrefab.transform.forward,dir);
		
			var rocket = Instantiate(arrowPrefab.transform,spawnPoint.position,rot);
			rocket.rigidbody.velocity = dir*rocketSpeed;
	}
	
	

}

function SelectWeapon(index : int)
{
	/*	
		switch(index)
		{
			case 1:
			{
				arrowPrefab = GameObject.Find("RamArrow").transform;
				break;
			
			}
			case 2:
			{
				arrowPrefab = GameObject.Find("cubeBullet").transform;
				break;
			
			}
			default:
				Debug.Log("No Other Weapon Available");
		}
	*/
	if(index == 0)
	{
		arrowPrefab = GameObject.Find("RamArrow");
	
	}
	else if (index == 1)
	{
		arrowPrefab = GameObject.Find("cubeBullet");
	
	}
	else
	{
		Debug.Log("No Other Weapon Available");
	
	}
}